﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PickupSystem : MonoBehaviour
{
    float currentTime = 0f;
    [SerializeField] float startingTime = 100f;  //controls how long timer is
    [SerializeField] Text countdownText;
    [SerializeField] Text startText;

    private double _doubleClickTimeLimit = 0.25f;

    public Text PointText;
    int PointCount;
    private void Start()
    {
        StartCoroutine(ClickListener());
        PointCount = 0;
        SetPointCount();
        currentTime = startingTime;
        Object.Destroy(startText, 3.0f);

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("rim"))
        {
            PointCount += 1;
            SetPointCount();
            this.transform.Reset();
            currentTime += 1;
        }
        else if (other.gameObject.CompareTag("rim2"))
        {
            PointCount += 2;
            SetPointCount();
            this.transform.Reset();
            currentTime += 2;
        }
        else if (other.gameObject.CompareTag("rim3"))
        {
            PointCount += 3;
            SetPointCount();
            this.transform.Reset();
            currentTime += 3;
        }
    }
    void SetPointCount()
    {
        PointText.text = "Points: " + PointCount.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        currentTime -= 1 * Time.deltaTime;
        countdownText.text = currentTime.ToString("0");

        if (currentTime <= 0)
        {
            SceneManager.LoadScene("Level1");
        }
    }
    private IEnumerator ClickListener()
    {
        
        while (enabled)
        {
            if (Input.GetKeyDown(KeyCode.Z))
            {
                yield return ButtonClick();
            }
            yield return null;
        }
    }
    private IEnumerator ButtonClick()
    {
        yield return new WaitForEndOfFrame();

        float timeCount = 0;
        while (timeCount < _doubleClickTimeLimit)
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                DoubleClick();
                yield break;
            }
            timeCount += Time.deltaTime;
            yield return null;
        }
        SingleClick();
    }
    private void DoubleClick()
    {
        //this.transform.Reset();
        Debug.Log("Double Click");
    }
    private void SingleClick()
    {
        Debug.Log("Single Click");
    }
}

